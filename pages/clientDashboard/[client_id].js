import React, { useEffect, useState } from 'react';
import Layout from '../components/layout';
import { useRouter } from 'next/router';
import Image from 'next/image';


import ImgFacebook from '../../public/Dashhoard/facebook.svg'
import ImgIg from '../../public/Dashhoard/instagram.svg'
import ImgTiktok from '../../public/Dashhoard/tikTok.svg'
import ImgYoutube from '../../public/Dashhoard/youtube.svg'
import ImgPerfil from '../../public/DashoardClient/perfil.svg'
import ImgPublic from '../../public/Calendar/public.svg'
import TableDashoardCli from "./Components/tableDashoardCli"
import ModalPlans from "./Components/ModalPlans"
import CounterDashoard from './Components/counterDashoard';
import CounterPlansActiveAll from './Components/counterPlansActiveAll';
import CounterPlansAll from './Components/counterPlansAll';



export default function DashoardClient() {
    
  const [showDots, setShowDots] = useState(true);
  const [reloadPlans, setReloadPlans] = useState(false);
  const [data, setData] = useState([ ]);

  const router = useRouter();
  const {client_id} = router.query;

  useEffect(() => {

    if(router.isReady){

    }
    
}, [router.isReady])

  return (
        <div className="ml-[30px] mr-[30px] mt-[45px]"> 
            <div className='grid grid-cols-12'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                    <div className='grid grid-cols-12'>
                        <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                            <div className='text-[20px] md:text-[32px] lg:text-[36px] text-[#000000] font-bold'>Dashboard Client {client_id}</div>
                            <div className="flex flex-row">
                                <div className="pr-[5px]">
                                    <Image
                                    src={ImgFacebook}
                                    layout='fixed'
                                    alt='ImgFacebook'
                                    width={38}
                                    height={38}
                                />
                                </div>

                                <div className="pr-[5px]">
                                    <Image
                                    src={ImgIg}
                                    layout='fixed'
                                    alt='ImgIg'
                                    width={38}
                                    height={38}
                                    
                                />
                                </div>

                                <div className="pr-[5px]">
                                    <Image
                                    src={ImgTiktok}
                                    layout='fixed'
                                    alt='ImgTiktok'
                                    width={38}
                                    height={38}
                                    
                                    />
                                </div>
                                <div className="">
                                    <Image
                                    src={ImgYoutube}
                                    layout='fixed'
                                    alt='ImgYoutube'
                                    width={38}
                                    height={38}

                                    />
                                </div>
                            
                            
                                </div>
                                
                            </div>
                            
                        </div >
                    </div >
                </div >

            <div className='grid grid-cols-12 mt-[20px]'>
                <div className='col-span-12 md:col-span-12 lg:col-span-4'>
                   <div className='grid grid-cols-12'>
                        <div className='col-span-12 md:col-span-12 lg:col-span-12'>
                            <div className="flex flex-row">
                                <div className="">
                                    <Image
                                       className='rounded-full'
                                        src={ImgPerfil}
                                        layout='fixed'
                                        alt='ImgPerfil'
                                        width={110}
                                        height={110}

                                        />

                                </div>
                                <div className="pl-[8px]">
                                    <div className="font-semibold text-[16px]" >Slogan Souvenirs</div>
                                    <div className="text-[15px]" >Client since: Feb 2, 20220</div>
                                    <div className="text-[15px]" >Has plan: <span className="text-[#643DCE] font-semibold">YES</span></div>
                                    <div className="font-semibold text-[16px]">YES SMART START CUSTOM</div>
                                </div>

                            </div>
                            
                        </div>
                   </div>

                </div>

                <div className='col-span-12 md:col-span-12 lg:col-span-8'>
                    <div className="text-[18px] font-medium mb-[10px]">
                        Accounts status  <span className="text-[13px] font-normal">- Total</span>
                    </div>
                    <div className="grid grid-cols-12 gap-4">
                        <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                            <div>
                            <div className="flex flex-row">
                                <div className="text-[32px] font-semibold text-[#000000]">
                                {router.isReady && 
                                    <CounterPlansActiveAll 
                                    client_id={client_id}
                                    />
                                } 
                                </div>
                                
                            </div>
                            <div className="text-[10px] text-[#000000]">ACTIVE PLANS</div>
                            </div>
                            <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                            
                            </div>
                        </div>
                        <div className='col-span-12 md:col-span-6 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                            <div>
                            <div className="flex flex-row">
                                <div className="text-[32px] font-semibold text-[#000000]">
                                {router.isReady && 
                                    <CounterPlansAll 
                                    client_id={client_id}
                                    />
                                }
                                </div>
                                    
                            </div>
                            <div className="text-[10px] text-[#000000]">PLANS TOTAL</div>
                            </div>
                            <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                            
                            </div>
                        </div>
                        <div className='col-span-12 md:col-span-12 lg:col-span-4 flex flex-row  bg-[#F3F3F3] rounded-[16px] shadow-md  justify-between p-[10px]'>
                            <div>
                            <div className="flex flex-row">
                                <div className="text-[32px] font-semibold text-[#000000]"></div>
                                    
                            </div>
                            <div className="text-[10px] text-[#000000]">ADS TOTAL</div>
                            </div>
                            <div className='w-[48px] h-[48px] bg-[#FFFFFF] rounded-[16px]'>
                            
                            </div>
                        </div>
                    </div>
                    
                </div>

            </div>


            <div className="grid grid-cols-12 gap-6">
                
                <div className='col-span-12 md:col-span-12 lg:col-span-7'>
                    
                    <div className="text-[18px] font-medium mt-[30px]">
                    Accounts status  <span className="text-[13px] font-normal">- Total</span>
                    </div>

                    <div className="grid grid-cols-12">
                        <div className='col-span-12 md:col-span-12 lg:col-span-12'>

                        {/* <CharDashoard/> */}

                        </div>
                        
                    </div>
                    
                    

                </div>

                <div className='col-span-12 md:col-span-12 lg:col-span-5'>
                    <div className='grid grid-cols-12 gap-4'>
                        <div className="col-span-12 md:col-span-6 lg:col-span-6">
                    
                            <div className="text-[18px] font-medium mt-[30px] mb-[10px]">
                                Latest post
                            </div>
                   
                            <div className='col-span-12 md:col-span-12 lg:col-span-12 border-[2px] border-[#D9D9D9] rounded-[16px] pl-[15px] pt-[10px] pr-[15px] pb-[10px]'>
                                <div className="flex flex-row">
                                    <div className="w-[28px] h-[28px] rounded-full bg-[#643DCE]">

                                    </div>
                                    <div className="pl-[10px]">

                                    <div className='text-[12px] font-semibold'>official_distributor</div>
                                    <div className="text-[8px]">Feb 2, 2022 11:20 P.M.</div>

                                    </div>

                                </div>
                                <div className='mt-[15px]'>
                                    <Image
                                    src={ImgPublic}
                                    layout='fixed'
                                    alt='ImgPublic'
                                    width={170}
                                    height={170}
                                    
                                    />
                                </div>
                                <div className="text-[10px]">
                                    #Italia tiene sus brillantes representantes en el pabellón europeo ¡Visítanos en la #Expocruz! 💚
                                </div>

                            </div>
                        
                        </div>

                        <div className="col-span-12 md:col-span-6 lg:col-span-6">

                            <div className="text-[18px] font-medium mt-[30px] mb-[10px]">
                            Next scheduled post
                            </div>
                            <div className='grid grid-cols-12'>
                                <div className='col-span-12 md:col-span-12 lg:col-span-12 border-[2px] border-[#D9D9D9] rounded-[16px] pl-[15px] pt-[10px] pr-[15px] pb-[10px]'>
                                    <div className="flex flex-row">
                                        <div className="w-[28px] h-[28px] rounded-full bg-[#643DCE]">

                                        </div>
                                        <div className="pl-[10px]">

                                        <div className='text-[12px] font-semibold'>official_distributor</div>
                                        <div className="text-[8px]">Feb 2, 2022 11:20 P.M.</div>

                                        </div>

                                    </div>
                                    <div className='mt-[15px]'>
                                        <Image
                                        src={ImgPublic}
                                        layout='fixed'
                                        alt='ImgPublic'
                                        width={170}
                                        height={170}
                                        
                                        />
                                    </div>
                                    <div className="text-[10px]">
                                        #Italia tiene sus brillantes representantes en el pabellón europeo ¡Visítanos en la #Expocruz! 💚
                                    </div>

                                </div>
                                
                            </div>
                        </div>

                    </div>
                </div>

            </div>

            <div className='grid grid-cols-12 mt-[30px]'>
                <div className='col-span-12 md:col-span-12 lg:col-span-12 flex flex-row justify-between'>
                    <div className='text-[17px] md:text-[15px] lg:text-[17px] text-[#000000] font-semibold'>Plans</div>
                    
                    <div className='flex flex-row '>

                    {router.isReady && 
                    
                        <ModalPlans client_id={client_id} reloadPlans={reloadPlans} setReloadPlans={setReloadPlans}
                        />
                    }
                    
                    </div>
                    
                </div>

                <div className='col-span-12 md:col-span-12 lg:col-span-12 mt-[30px]'>

                    {router.isReady && 

                    <TableDashoardCli client_id={client_id} reloadPlans={reloadPlans} setReloadPlans={setReloadPlans}
                    />
                    }

                </div>
            
            </div >
        </div>
    )
}

DashoardClient.getLayout = function getLayout(page){
    return (
      <Layout>{page}</Layout>
    )
  }