import React from 'react';
import Spinner from 'react-activity/dist/Spinner';
import Image from 'next/image'
import 'react-activity/dist/Spinner.css';
import { useEffect, useState } from 'react'

 /////// IMAGENES ////////////////////
 import ImgFacebook from '../../../public/Dashhoard/facebook.svg'
 import ImgGmail from '../../../public/Dashhoard/gmail.svg'
 import ImgIg from '../../../public/Dashhoard/instagram.svg'
 import ImgMessenger from '../../../public/Dashhoard/messenger.svg'
 import ImgTelegram from '../../../public/Dashhoard/telegram.svg'
 import ImgTiktok from '../../../public/Dashhoard/tikTok.svg'
 import ImgTwitter from '../../../public/Dashhoard/twitter.svg'
 import ImgWhatsapp from '../../../public/Dashhoard/whatsapp.svg'
 import ImgYoutube from '../../../public/Dashhoard/youtube.svg'

const PriceAds = (props) => {

    const { client_plan_id} = props;

    const [isLoading, setIsLoading] = useState(true);
    const [data, setData] = useState([]);

    useEffect(() => {
        fetch('https://slogan.com.bo/roadie/clientsPlansPostsAds/all/' + client_plan_id)
            .then(response => response.json())
            .then(data => {
                if (data.status) {
                    console.log('PRICEAds: ' + data.data);
                    setData(data.data)
                } else {
                    console.error(data.error)
                }
                setIsLoading(false)
            })

    }, [])

    return (
        isLoading ?
        <>
            <div className='flex justify-center items-center' style={{ width: '100%', height: 70 }}>
                <Spinner color="#582BE7" size={17} speed={1} animating={true} style={{ marginLeft: 'auto', marginRight: 'auto' }} />
            </div>

        </>
        :
        <div className='rounded-[22.35px] pt-6 h-full w-full overflow-auto'>
            <CounterAds data={data} client_plan_id={client_plan_id }/>
        </div>
 
    )
};

const CounterAds = (props) => {
    const { data } = props;
   


    return (
        <div>

            {data.map(row => {

                var counterAdsFacebook = ''
                var counterAdsTikTok = ''
                var counterAdsInstagram = ''
                var counterAdsYouTube = ''
                var counterAdsMailing = ''
                var counterAdsLinkedIn = ''
                var counterAdsTwitter = ''

                if(row._matchingData.ClientsPlansPosts.social_network  !== null){
                    switch (row._matchingData.ClientsPlansPosts.social_network ) {
                        case 'Facebook': 
                            counterAdsFacebook = counterAdsFacebook + 1
                            break;
                        case 'TikTok':
                            counterAdsTikTok = counterAdsTikTok + 1
                            break;
                        case 'Instagram':
                            counterAdsInstagram = counterAdsInstagram + 1
                        break;
                        case 'YouTube':
                            counterAdsYouTube = counterAdsYouTube + 1
                        break;
                        case 'Mailing':
                            counterAdsMailing = counterAdsMailing + 1
                        break;
                        case 'LinkedIn':
                            counterAdsLinkedIn = counterAdsLinkedIn + 1
                        break;
                        case 'Twitter':
                            counterAdsTwitter = counterAdsTwitter + 1
                        break;
                            
                        default:
                            break;
                        }
                    }
            
                return (
                    <div className="mt-[20px] grid grid-cols-12 gap-3"
                        key={row.id}>
                        <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                            <Image
                                src={ImgFacebook}
                                layout='fixed'
                                alt='ImgFacebook'
                                width={56}
                                height={56}
                                
                            />
                            <div className="ml-[10px]">
                                <p className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">{counterAdsFacebook}</p>
                                <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                            </div>
                            
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                            <Image
                                src={ImgTelegram}
                                layout='fixed'
                                alt='ImgTelegram'
                                width={56}
                                height={56}
                                
                            />
                            <div className="ml-[10px]">
                                <p className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">{counterAdsTikTok}</p>
                                <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                            </div>
                            
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                            <Image
                                src={ImgIg}
                                layout='fixed'
                                alt='ImgIg'
                                width={56}
                                height={56}
                                
                            />
                            <div className="ml-[10px]">
                                <p className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">{counterAdsInstagram}</p>
                                <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                            </div>
                            
                        </div>
                        <div className='col-span-6 md:col-span-6 lg:col-span-3 flex flex-row'>
                            <Image
                                src={ImgYoutube}
                                layout='fixed'
                                alt='ImgYoutube'
                                width={56}
                                height={56}
                                
                            />
                            <div className="ml-[10px]">
                                <p className="text-[15px] md:text-[18px] lg:text-[18px] font-semibold text-[#000000]">{counterAdsYouTube}</p>
                                <div className="text-[13px] text-[#A7A7B7]">TOTAL ADS</div>
                            </div>
                        </div>

                    </div>

                )
                }
            )}
            
        </div>     
    );
};





export default PriceAds;