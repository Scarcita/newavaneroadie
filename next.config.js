/** @type {import('next').NextConfig} */
const nextConfig = {
  // reactStrictMode: true,
  reactStrictMode: false,
  // ReactStrictMode: false
  swcMinify: true,
  // images: {
  //   domains: ['logodownload.org', 'www.w3schools.com', 'slogan.com.bo'],
  // },

  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: '*',
      },
      {
        protocol: 'http',
        hostname: '*',
      },
    ],
  },

}

module.exports = nextConfig
